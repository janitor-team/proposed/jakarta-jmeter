#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=apache-jmeter-$2
TAR=../jakarta-jmeter_$2.orig.tar.xz

# clean up the upstream tarball
tar zxvf $3
rm $3
tar -c -J -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
